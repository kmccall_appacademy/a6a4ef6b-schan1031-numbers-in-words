class Integer
  def basenum(int)
    names = {
      0 => 'zero',
      1 => 'one',
      2 => 'two',
      3 => 'three',
      4 => 'four',
      5 => 'five',
      6 => 'six',
      7 => 'seven',
      8 => 'eight',
      9 => 'nine',
      10 => 'ten',
      11 => 'eleven',
      12 => 'twelve',
      13 => 'thirteen',
      14 => 'fourteen',
      15 => 'fifteen',
      16 => 'sixteen',
      17 => 'seventeen',
      18 => 'eighteen',
      19 => 'nineteen',
      20 => 'twenty',
      30 => 'thirty',
      40 => 'forty',
      50 => 'fifty',
      60 => 'sixty',
      70 => 'seventy',
      80 => 'eighty',
      90 => 'ninety',
      100 => 'hundred',
    }

    l = int.to_s.length
    s = ''
    if int<20
      return names[int]
    elsif l == 2
      b = int.divmod(10)
      return names[b[0]*10] if b[1] == 0
      return names[b[0]*10] + ' ' + names[b[1]] if b[1] != 0
    elsif l == 3
      b = int.divmod(100)
      c = b[1].divmod(10)
      return names[b[0]] + ' ' + names[100] if b[1] == 0
      return names[b[0]] + ' ' + names[100] + ' ' + names[c[0]*10] + ' ' + names[c[1]] if b[1] != 0 && b[1] > 20 && c[1] != 0
      return names[b[0]] + ' ' + names[100] + ' ' + names[c[0]*10] if b[1] != 0 && b[1] > 20 && c[1] == 0
      return names[b[0]] + ' ' + names[100] + ' ' + names[b[1]] if b[1] < 20 && b[1] > 0
    end
  end

  def numscale(int)
    l = int.to_s.length
    s = ''
    if l < 4
      return (basenum(int))
    elsif l > 3 && l < 7
      thousands = int.divmod(1000)
      return basenum(thousands[0]) + ' thousand' if thousands[1] == 0
      return basenum(thousands[0]) + ' thousand ' + numscale(thousands[1]) if thousands[1] != 0
    elsif l > 6 && l < 10
      millions = int.divmod(1_000_000)
      return basenum(millions[0]) + ' million' if millions[1] == 0
      return basenum(millions[0]) + ' million ' + numscale(millions[1]) if millions[1] != 0
    elsif l > 9 && l < 13
      billions = int.divmod(1_000_000_000)
      return basenum(billions[0]) + ' billion' if billions[1] == 0
      return basenum(billions[0]) + ' billion ' + numscale(billions[1]) if billions[1] != 0
    elsif l > 12
      trillions = int.divmod(1_000_000_000_000)
      return basenum(trillions[0]) + ' trillion' if trillions[1] == 0
      return basenum(trillions[0]) + ' trillion ' + numscale(trillions[1]) if trillions[1] != 0
    end

  end

  def in_words
    l = self.to_s.length
    numscale(self)

  end

end
